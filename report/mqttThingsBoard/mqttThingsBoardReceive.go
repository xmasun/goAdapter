package mqttThingsBoard

import (
	"goAdapter/setting"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

type MQTTThingsBoardReceiveFrameTemplate struct {
	Topic   string
	Payload []byte
}

type MQTTThingsBoardLogInDataTemplate struct {
	ProductKey string `json:"productKey"`
	DeviceName string `json:"deviceName"`
}

type MQTTThingsBoardLogInAckTemplate struct {
	ID      string                             `json:"id"`
	Code    int32                              `json:"code"`
	Message string                             `json:"message"`
	Data    []MQTTThingsBoardLogInDataTemplate `json:"data"`
}

type MQTTThingsBoardLogOutDataTemplate struct {
	Code       int32  `json:"code"`
	Message    string `json:"message"`
	ProductKey string `json:"productKey"`
	DeviceName string `json:"deviceName"`
}

type MQTTThingsBoardLogOutAckTemplate struct {
	ID      string                              `json:"id"`
	Code    int32                               `json:"code"`
	Message string                              `json:"message"`
	Data    []MQTTThingsBoardLogOutDataTemplate `json:"data"`
}

type MQTTThingsBoardReportPropertyAckTemplate struct {
	Code    int32  `json:"code"`
	Data    string `json:"-"`
	ID      string `json:"id"`
	Message string `json:"message"`
	Method  string `json:"method"`
	Version string `json:"version"`
}

//发送数据回调函数
func ReceiveMessageHandler(client MQTT.Client, msg MQTT.Message) {

	for k, v := range ReportServiceParamListThingsBoard.ServiceList {
		if v.GWParam.MQTTClient == client {
			receiveFrame := MQTTThingsBoardReceiveFrameTemplate{
				Topic:   msg.Topic(),
				Payload: msg.Payload(),
			}
			setting.ZAPS.Debugf("Recv TOPIC: %s", receiveFrame.Topic)
			setting.ZAPS.Debugf("Recv MSG: %s", receiveFrame.Payload)
			ReportServiceParamListThingsBoard.ServiceList[k].ReceiveFrameChan <- receiveFrame
		}
	}
}
